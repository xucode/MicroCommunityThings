package com.java110.core.dao;

import com.java110.entity.car.CarInoutDto;
import com.java110.entity.car.CarInoutTempAuthDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName ICarInoutServiceDao
 * @Description TODO
 * @Author wuxw
 * @Date 2020/5/15 21:02
 * @Version 1.0
 * add by wuxw 2020/5/15
 **/
@Mapper
public interface ICarInoutTempAuthServiceDao {

    /**
     * 保存临时车审核
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return 返回影响记录数
     */
    int saveCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto);

    /**
     * 查询临时车审核总数
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     */
    long queryCarInoutTempAuthCount(CarInoutTempAuthDto carInoutTempAuthDto);

    /**
     * 查询临时车审核信息
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return
     */
    List<CarInoutTempAuthDto> getCarInoutTempAuths(CarInoutTempAuthDto carInoutTempAuthDto);



    /**
     * 修改临时车审核信息
     *
     * @param carInoutTempAuthDto 临时车审核信息
     * @return 返回影响记录数
     */
    int updateCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto);

    /**
     * 删除指令
     *
     * @param carInoutTempAuthDto 指令id
     * @return 返回影响记录数
     */
    int delete(CarInoutTempAuthDto carInoutTempAuthDto);
}
